package com.adamkorzeniak.android.features;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.adamkorzeniak.android.features.popups.alert_dialog.AlertDialogActivity;
import com.adamkorzeniak.android.features.popups.notifications.NotificationActivity;
import com.adamkorzeniak.android.features.popups.toast.ToastActivity;
import com.adamkorzeniak.android.features.persistence.room.RoomFrameworkActivity;
import com.adamkorzeniak.android.features.persistence.shared_preferences.SharedPreferencesActivity;
import com.adamkorzeniak.android.features.persistence.sqlite.SQLiteActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button_shared_preferences).setOnClickListener(v -> {
            final Intent intent = new Intent(MainActivity.this, SharedPreferencesActivity.class);
            startActivity(intent);
        });
        findViewById(R.id.button_sqlite).setOnClickListener(v -> {
            final Intent intent = new Intent(MainActivity.this, SQLiteActivity.class);
            startActivity(intent);
        });
        findViewById(R.id.button_room_framework).setOnClickListener(v -> {
            final Intent intent = new Intent(MainActivity.this, RoomFrameworkActivity.class);
            startActivity(intent);
        });



        findViewById(R.id.button_toast).setOnClickListener(v -> {
            final Intent intent = new Intent(MainActivity.this, ToastActivity.class);
            startActivity(intent);
        });
        findViewById(R.id.button_notifications).setOnClickListener(v -> {
            final Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
            startActivity(intent);
        });
        findViewById(R.id.button_alert_dialog).setOnClickListener(v -> {
            final Intent intent = new Intent(MainActivity.this, AlertDialogActivity.class);
            startActivity(intent);
        });

    }
}