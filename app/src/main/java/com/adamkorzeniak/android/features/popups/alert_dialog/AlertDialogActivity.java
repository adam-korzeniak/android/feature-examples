package com.adamkorzeniak.android.features.popups.alert_dialog;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.adamkorzeniak.android.features.R;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class AlertDialogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_dialog);

        AlertDialog.Builder alert1 = new AlertDialog.Builder(this)
                .setTitle("Simple Alert Dialog!")
                .setMessage("Now you know how to use a new component!")
                .setIcon(android.R.drawable.ic_delete)
                .setPositiveButton(android.R.string.ok, (a, b) ->
                        Toast.makeText(this, "Ok", Toast.LENGTH_SHORT).show())
                .setNegativeButton(android.R.string.cancel, null);


        String[] items = {"First Item", "Second Item", "Third Item"};
        AlertDialog.Builder alert2 = new AlertDialog.Builder(this)
                .setTitle("Simple Alert Dialog!")
                .setItems(items, (dialog, which) -> Toast.makeText(this, items[which] + " is selected", Toast.LENGTH_SHORT)
                        .show());


        String[] choice = {"First Item", "Second Item", "Third Item"};
        AtomicReference<String> chosen = new AtomicReference<>();
        AlertDialog.Builder alert3 = new AlertDialog.Builder(this)
                .setTitle("Simple Alert Dialog!")
                .setSingleChoiceItems(choice, -1, (dialog, which) -> chosen.set(choice[which]))
                .setPositiveButton(android.R.string.ok, (a, b) ->
                        Toast.makeText(this, chosen.get(), Toast.LENGTH_SHORT).show());

        findViewById(R.id.button1).setOnClickListener(v -> alert1.show());
        findViewById(R.id.button2).setOnClickListener(v -> alert2.show());
        findViewById(R.id.button3).setOnClickListener(v -> alert3.show());
    }
}