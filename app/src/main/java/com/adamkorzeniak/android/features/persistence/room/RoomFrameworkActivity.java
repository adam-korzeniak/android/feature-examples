package com.adamkorzeniak.android.features.persistence.room;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.adamkorzeniak.android.features.Application;
import com.adamkorzeniak.android.features.R;

import java.util.List;

public class RoomFrameworkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_framework);

        AppDatabase database = ((Application) getApplication()).getDatabase();
        PersonDao personDao = database.getPersonDao();


        Person adam = new Person("Adam", 30);
        Person magda = new Person("Magda", 31);
        personDao.insertAll(adam, magda);

        List<Person> people = personDao.getAll();

        List<Person> person = personDao.findByName("ada", 30);

        Person personUpdate = people.get(0);
        personUpdate.setName("XXXX");
        int update = personDao.update(personUpdate);

        personDao.delete(magda);
    }
}