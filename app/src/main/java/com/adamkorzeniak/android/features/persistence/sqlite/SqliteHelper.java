package com.adamkorzeniak.android.features.persistence.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SqliteHelper extends SQLiteOpenHelper {

    public SqliteHelper(@Nullable Context context, int version) {
        super(context, "app.db", null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE people(_id INTEGER PRIMARY KEY, name VARCHAR, birth DATE)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE people");
        db.execSQL(
                "CREATE TABLE people(_id INTEGER PRIMARY KEY, name VARCHAR, birth DATE)");
    }
}
