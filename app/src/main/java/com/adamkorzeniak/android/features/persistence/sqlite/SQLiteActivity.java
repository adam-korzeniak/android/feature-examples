package com.adamkorzeniak.android.features.persistence.sqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

import com.adamkorzeniak.android.features.R;

import java.time.LocalDate;
import java.util.List;

public class SQLiteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite);

        SQLiteDatabase db = new SqliteHelper(this, 5).getWritableDatabase();
        PersonRepository personDb = new PersonRepository(db);

        long adam = personDb.insert("Adam", LocalDate.of(1990, 11, 7));
        long magda = personDb.insert("Magda", LocalDate.of(1988, 8, 24));

        List<Person> people = personDb.all();

        Person person = personDb.singleOrNull(1L);

        Person personUpdate = people.get(0);
        personUpdate.setName("XXXX");
        personDb.update(personUpdate);

        personDb.delete(magda);
    }
}