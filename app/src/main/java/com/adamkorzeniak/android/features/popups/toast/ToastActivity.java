package com.adamkorzeniak.android.features.popups.toast;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.widget.Toast;

import com.adamkorzeniak.android.features.R;

public class ToastActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toast);

        String text = "Here can be your text";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(this, text, duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.addCallback(new Toast.Callback() {
            public void onToastShown() {
                // Your code here
            }

            public void onToastHidden() {
                // Your code here
            }
        });

        toast.show();
    }
}