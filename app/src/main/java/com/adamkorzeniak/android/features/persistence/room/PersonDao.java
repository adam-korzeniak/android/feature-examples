package com.adamkorzeniak.android.features.persistence.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PersonDao {

    @Query("SELECT * FROM person")
    List<Person> getAll();

    @Query("SELECT * FROM person WHERE id IN (:ids)")
    List<Person> loadAllByIds(int[] ids);

    @Query("SELECT * FROM person " +
            "WHERE name LIKE '%' || :name || '%' AND age = :age")
    List<Person> findByName(String name, Integer age);

    @Insert
    void insertAll(Person... people);

    @Update
    int update(Person people);

    @Delete
    void delete(Person person);

    @Query("DELETE FROM person")
    void deleteAll();
}
