package com.adamkorzeniak.android.features.popups.notifications;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.adamkorzeniak.android.features.MainActivity;
import com.adamkorzeniak.android.features.R;

public class NotificationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);


        final String CHANNEL_ID = "deliveryStatus";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String name = "Delivery status";;
            String descriptionText = "Your delivery status";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(descriptionText);

            NotificationManager notificationManager = (NotificationManager)  getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                1,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.astronomy)
                .setContentTitle("Delivery")
                .setContentText("Food is ready!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentText("Food is ready!")
                .setStyle(new NotificationCompat.BigTextStyle())
                .setAutoCancel(true) // automatically remove notification after click
                .setTimeoutAfter(3600_000) // remove notification after some time in milliseconds
                .setOnlyAlertOnce(true)
                .setContentIntent(pendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);

//        notificationManager.cancel(1);
//        notificationManager.cancelAll();


    }
}