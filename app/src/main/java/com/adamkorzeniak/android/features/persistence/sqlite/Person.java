package com.adamkorzeniak.android.features.persistence.sqlite;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    private long id;
    private String name;
    private LocalDate birth;
}
