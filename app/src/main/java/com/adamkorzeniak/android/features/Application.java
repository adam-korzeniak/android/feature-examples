package com.adamkorzeniak.android.features;

import androidx.room.Room;

import com.adamkorzeniak.android.features.persistence.room.AppDatabase;

public class Application extends android.app.Application {

    public AppDatabase getDatabase() {
        return Room.databaseBuilder(
                this,
                AppDatabase.class,
                "database.db"
        )
                .allowMainThreadQueries()
                .build();
    }
}
