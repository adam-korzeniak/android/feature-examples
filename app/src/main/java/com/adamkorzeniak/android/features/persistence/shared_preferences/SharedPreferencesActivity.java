package com.adamkorzeniak.android.features.persistence.shared_preferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.adamkorzeniak.android.features.R;

public class SharedPreferencesActivity extends AppCompatActivity {

    private static final String PREFERENCES_NAME = "app_preferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);

        example();
    }

    private void example() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("text", "value");
        editor.putInt("number", 111);
        editor.putInt("number", 111).apply();
        editor.apply();

        preferences.getString("text", "defaultValue");
        preferences.getInt("number", 0);

        editor.remove("number");
        editor.clear();
    }
}