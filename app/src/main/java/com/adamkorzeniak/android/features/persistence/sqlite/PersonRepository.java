package com.adamkorzeniak.android.features.persistence.sqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.io.Closeable;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lombok.Synchronized;

public class PersonRepository implements Closeable {

    private final SQLiteDatabase db;
    private final SQLiteStatement insertStatement;
    private final SQLiteStatement updateStatement;
    private final SQLiteStatement deleteStatement;

    private final String[] personCols = {"_id", "name", "birth"};

    public PersonRepository(SQLiteDatabase db) {
        this.db = db;
        this.insertStatement = db.compileStatement("INSERT INTO people (name, birth) VALUES (?, ?)");
        this.updateStatement = db.compileStatement("UPDATE people SET name = ?, birth = ? WHERE _id = ?");
        this.deleteStatement = db.compileStatement("DELETE FROM people WHERE _id = ?");
    }

    @Synchronized
    public long insert(String name, LocalDate date) {
        insertStatement.bindString(1, name);
        insertStatement.bindLong(2, date.toEpochDay());
        return insertStatement.executeInsert();
    }

    public Person singleOrNull(Long id) {
        Cursor cursor = db.query("people", personCols, "_id = ?", new String[]{id.toString()}, null, null, null);
        if (cursor.moveToFirst()) {
            Person person = extractData(cursor);
            cursor.close();
            return person;
        }
        cursor.close();
        return null;
    }

    @Override
    public void close() throws IOException {
        insertStatement.close();
        updateStatement.close();
        deleteStatement.close();
    }


    public void update(Person person) {
        updateStatement.bindString(1, person.getName());
        updateStatement.bindLong(2, person.getBirth().toEpochDay());
        updateStatement.bindLong(3, person.getId());
        updateStatement.executeUpdateDelete();
    }

    public void delete(Person person) {
        deleteStatement.bindLong(1, person.getId());
        deleteStatement.executeUpdateDelete();
    }

    public void delete(Long id) {
        deleteStatement.bindLong(1, id);
        deleteStatement.executeUpdateDelete();
    }

    private Person extractData(Cursor cur) {
        return new Person(
                cur.getLong(0),
                cur.getString(1),
                LocalDate.ofEpochDay(cur.getLong(2)));
    }

    public List<Person> all() {
        Cursor cursor = db.query("people", personCols, null, null, null, null, null);

        List<Person> people = new ArrayList<>();

        while (cursor.moveToNext()) {
            people.add(extractData(cursor));
        }
        cursor.close();
        return people;

    }
}